//Ce programme permet d'afficher l'alphabet en majuscule
//On utilisera l'unicode de chaque lettre
//Romain Blauwblomme Le 17/03/2020 à 10h38

function AfficherAlphabet() {
//Fonction de conversion de l'unicode vers une chaine de caractère
    let code = 65;      //unicode de la lettre A
    let res;            //conversion de l'unicode en caractère
    let affFinal = "";  //Affichage final
    //Conversion de chaque lettre
    for (code = 65; code <= 90; code++) {       //Unicode de A à Z = 65 à 90
        res = String.fromCharCode(code);        //Conversion en charactère
        affFinal += res + " - ";                //concaténation pour affichage final
    }
    return affFinal;
}
let affFinal = "";                                      //Affichage final
affFinal = AfficherAlphabet();                          //Utilisation de la fonction
affFinal = affFinal.substring(0, affFinal.length - 3);  //Pour enlevé le " - " à la fin de la chaine de caractère
alert(affFinal);                                        //Affichage final