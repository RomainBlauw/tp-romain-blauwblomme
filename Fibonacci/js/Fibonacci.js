/* 
    Ce programme sert à afficher la suite de Fibonacci avec autant de nombre qu'on le souhaite

    Romain Blauwblomme, le 16/03/2020 à 15h55
*/

//Fonction du calcul de la suite de fibonacci
//un paramètre, la suite commence forcément par 0 et 1
// 
function calculerSuiteFibonacci(prmNombreMax){  

    let premNombre = 0;     //Premier nombre pour le calcul (forcément 0 au départ)
    let secNombre = 1;      //Second nombre pour le calcul (forcément 1 au départ)
    let nombre = 0;         //résultat des additions des deux nombres précedents
    let affFinal = "";      //Affichage final
    let nombreMax = prmNombreMax;   //le nombre de termes de la suite défini par le paramètre de la fonction
    
    nombreMax = nombreMax - 2;      //on ne compte pas les 2 premiers termes
    affFinal = premNombre + " - " + secNombre + " - ";  //On commence par 0 et 1
    
    for (let i = 0; i < nombreMax; i++) {       //On sait combien de fois on doit le répéter
        if (i < nombreMax - 1) {                //Pour tous les nombres sauf le derniers
            nombre = premNombre + secNombre;    //Addition des deux nombres précedents
            premNombre = secNombre;             //l'ancien 2eme nombre devient le premier pour la suite
            secNombre = nombre;                 //le dernier nombre de la suite devient le secNombre pour le calcul du suivant
            affFinal += nombre + " - ";         //concaténation de l'affichage final
        }
        else {
            //Pour le dernier nombre à afficher, même procédé                                  
            nombre = premNombre + secNombre;    
            premNombre = secNombre;
            secNombre = nombre;
            affFinal += nombre;
        }
    }
    return affFinal;    
}

let affFinal="";        //Affichage final 
let nombreMax = prompt("Combien de nombre voulez vous affichez ?"); //Nombre de termes à afficher

affFinal = calculerSuiteFibonacci(nombreMax);   //Utilisation de la fonction
alert(affFinal);                                //Affichage final