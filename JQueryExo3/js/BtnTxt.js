/*
  Ce programme sert à générer du texte sur une page HTML lors d'un clic
  sur bouton grace au JQuery
  
  Romain Blauwblomme le 26/03/2020
*/
$(document).ready(function () {
    //Code à exécuter après le chargement du DOM
    //Execution de la fonction anonyme lors du clic sur le bouton 
    $("#btn1").click(       
        function () {
            $("#maDiv1").html("Coucou du Jquery grace a ce bouton");    //Edition de la DIV 
        }
    );
});